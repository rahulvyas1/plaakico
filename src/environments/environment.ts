// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  APIPath: 'http://34.253.116.205/api/',
  NodeAPIPath: 'http://pnode.dexa.site/',
  PLAAKAPIPath: 'http://icoapi.plaak.com/v1/',
  stripeKey: 'pk_test_xBRedCkFCo7rhU5s1TtdAT69',
  EthplorerPath: 'https://api.etherscan.io/api?',
};
