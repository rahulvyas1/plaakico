import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { LoginService } from '../core/service/login.service';
import { MessageService } from '../core/service/message.service';
import { CommonConstant } from '../core/constant/common.constant';
import { AuthService } from '../core/service/auth.service';
import { ValidationService } from '../shared/components/validation-message/validation-service';
import { Router } from '@angular/router';
import { UserService } from '../core/service/user.service';
import { User } from '../model/user';
import { CommonService } from '../core/service/common.service';

import { AuthService as SocialAuthService } from "angular4-social-login";
import { SocialUser } from "angular4-social-login";

import { FacebookLoginProvider, GoogleLoginProvider } from "angular4-social-login";
import { BaseService } from '../core/service/base.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	model = 'signin';

	public email;
	public password;
	emailFormControl = new FormControl('', [
		Validators.required
	]);

	passwordFormControl = new FormControl('', [
		Validators.required
	]);
	frmRegister: FormGroup;
	frmLogin: FormGroup;
	frmRestpsdInitil: FormGroup;
	frmRestpsdFinal: FormGroup;
	isFormSubmitted: boolean;
	isLoginFormSubmitted: boolean;
	isRestFormSubmitted: boolean;
	errorMessage: string;
	error: boolean;
	registrationSuccess: boolean;
	loginErrorMessage: string;
	restErrorMessage: string;
	loginError: boolean;
	restError: boolean;
	referralCode: string;

	user: SocialUser;
	loggedIn: boolean;


	constructor(
		private baseService:BaseService,
		private loginService: LoginService, private formBuilder: FormBuilder, private commonService: CommonService,
		private authService: AuthService, private router: Router, private userService: UserService,
		private socialAuthService: SocialAuthService) { }

	ngOnInit() {
		this.createLoginForm();
		this.createRegistrationForm();
		this.createRestPasswordInitilForm();
		this.createRestPasswordFinalForm();
		this.getReferralCode();
		this.socialAuthService.authState.subscribe((user) => {
			this.user = user;
			this.loggedIn = (user != null);
		});
		
	}

	createLoginForm() {
		this.frmLogin = this.formBuilder.group({
			username: ['', [Validators.required]],
			password: ['', [Validators.required]],
			rememberMe: []
		});
	}

	createRegistrationForm() {
		this.frmRegister = this.formBuilder.group({
			email: ['', [Validators.required, ValidationService.emailValidator]],
			password: ['', [Validators.required, Validators.minLength(6)]],
			comparePasswrod: ['', [Validators.required, ValidationService.comparePassword]],
			termsAndCondition: []
		});
	}
	createRestPasswordInitilForm() {
		this.frmRestpsdInitil = this.formBuilder.group({
			email: ['', [Validators.required, ValidationService.emailValidator]]
		});
	}
	createRestPasswordFinalForm() {
		this.frmRestpsdFinal = this.formBuilder.group({		
			code: ['', [Validators.required]],
			password: ['', [Validators.required, Validators.minLength(6)]],
			comparePasswrod: ['', [Validators.required, ValidationService.comparePassword]],
		});
	}
	getReferralCode() {
		this.referralCode = this.commonService.getReferralCode();
	}

	login() {
		this.isLoginFormSubmitted = true;
		if (this.frmLogin.invalid) {
			return;
		}
		let user = this.frmLogin.value;
		this.loginError = false;
		this.loginService.login(user)
			.subscribe(login_result => {
				this.authService.saveToken(login_result.id_token);
				this.userService.getUser()
					.subscribe((user: User) => {
						this.authService.saveUser(user);
						this.router.navigate(['/dashboard']);
					})
			},
			error => {
				this.loginError = true;
				switch (error.status) {
					case 401:
						this.loginErrorMessage = 'Email and/or password are invalid.';
						break;

					default:
						this.loginErrorMessage = 'Something went wrong';
						break;
				}
			});
	}

	registerUser() {
		this.isFormSubmitted = true;
		if (this.frmRegister.invalid) {
			return;
		}
		let user = {
			email: this.frmRegister.value.email,
			password: this.frmRegister.value.password,
			langKey: CommonConstant.languageKey,
			login: this.frmRegister.value.email,
			phoneNumber: '1234567891'
		};
		this.error = false;

		this.loginService.register(user, this.referralCode)
			.subscribe(login_result => {
				this.registrationSuccess = true;
			},
			error => {
				this.error = true;
				switch (error.status) {
					case 400:
						if (error.error) {
							switch (error.error.type) {
								case CommonConstant.LOGIN_ALREADY_USED_TYPE:
									this.errorMessage = 'Login name already registered, Please choose another.';
									break;
								case CommonConstant.EMAIL_ALREADY_USED_TYPE:
									this.errorMessage = 'Login name already registered, Please choose another.';
									break;
								default:
									this.errorMessage = 'Something went wrong';
									break;
							}

						} else {
							this.errorMessage = 'Something went wrong';
						}
						break;

					default:
						this.errorMessage = 'Something went wrong';
						break;
				}
			});
	}

	goToLogin() {
		this.registrationSuccess = false;
		this.loginError = false;
		this.createRegistrationForm();
		this.model = 'signin';
	}

	signInWithGoogle(): void {
		this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
		// this.router.navigateByUrl('dashboard');
		this.router.navigate(['/dashboard']);
	}

	signInWithFB(): void {
		this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
	}

	signOut(): void {
		this.socialAuthService.signOut();
	}

	//#region rest password
	showRestForm() {
		this.model = 'rest';
	}
	RequestRestPassword() {

		this.isRestFormSubmitted = true;
		if (this.frmRestpsdInitil.invalid) {
			return;
		}

		this.restError = false;
		const data={
			mail:this.frmRestpsdInitil.value.email
		}
		this.loginService.restPassword(this.frmRestpsdInitil.value.email)
			.subscribe(login_result => {
				this.frmRestpsdFinal.value.email=this.frmRestpsdInitil.value.email;
				this.showRestPasswordFinal();
			},
			error => {
				this.restError = true;
				this.restErrorMessage = 'Something went wrong';

			});
	}

	showRestPasswordFinal() {
		this.model = 'restFinal';
	}
	restPassword(){
		this.isRestFormSubmitted = true;
		if (this.frmRestpsdFinal.invalid) {
			return;
		}

		this.restError = false;
		const data={
			key: this.frmRestpsdFinal.value.code,
			newPassword: this.frmRestpsdFinal.value.password
		  }
		this.loginService.restPasswordFinal(this.frmRestpsdFinal.value.email)
			.subscribe(login_result => {
				console.log(login_result);				
				this.model='signin';
			},
			error => {
				this.restError = true;
				this.restErrorMessage = 'Something went wrong';

			});
	}
	//#end region 
}
