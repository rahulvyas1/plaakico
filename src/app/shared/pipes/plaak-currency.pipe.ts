import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'plaakcurrency'
})
export class PlaakCurrencyPipe implements PipeTransform {
    public transform(value) {

        if (!value) return '0 PLK';
        return `${value} PLK`;

    }
}