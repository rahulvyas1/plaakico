export class ValidationService {
    static getValidatorErrorMessage(validatorName: string, validatorValue?: any, message?: string) {
        let config = {
            'required': `${message} is required`,
            'invalidEmailAddress': 'Invalid email address',
            'minlength': `Minimum length ${validatorValue.requiredLength}`,
            'mismatchedPassword': 'Password is not matching',
            'invalidETHAddress': 'Invalid Address'
        };

        return config[validatorName];
    }

    static emailValidator(control) {
        // RFC 2822 compliant regex
        if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return null;
        } else {
            return { 'invalidEmailAddress': true };
        }
    }


    static comparePassword(control) {
        if (!control.root || !control.root.controls) {
            return null;
        }
        const exactMatch = control.root.controls.password.value === control.value;
        return exactMatch ? null : { mismatchedPassword: true };
    }

    static validAddress(control) {
        if (control.value) {
            let strValue: string = control.value.trim();
            if (strValue.startsWith('0x')) {
                strValue = strValue.substring(2);
            }
            if (/^[0-9a-f]{38,40}$/i.test(strValue)) {
                return null;
            }
        }
        return { 'invalidETHAddress': true };
    }




}