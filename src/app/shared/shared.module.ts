

import { NgModule } from '@angular/core';
import { PasswordStrengthBar } from './components/password-strength/password-strength-bar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValidationMessage } from './components/validation-message/validation-message';
import { CommonModule } from '@angular/common';
import { ObjectToArrayPipe } from './pipes/object-to-array.pipe';
import { SearchPipe } from './pipes/search.pipe';
import { HeaderBarComponent } from '../secure/header-bar/header-bar.component';
import { RouterModule } from '@angular/router';
import { PlaakCurrencyPipe } from './pipes/plaak-currency.pipe';
import { ClickOutsideDirective } from './directives/click-outside.directive';


const sharedModule = [CommonModule, FormsModule, ReactiveFormsModule, RouterModule];

const shared = [ValidationMessage, PasswordStrengthBar, ObjectToArrayPipe,
    SearchPipe, HeaderBarComponent, PlaakCurrencyPipe, ClickOutsideDirective];

@NgModule({
    imports: [
        ...sharedModule
    ],
    declarations: [
        ...shared
    ],
    exports: [
        ...sharedModule,
        ...shared
    ]
})
export class SharedModule { }
