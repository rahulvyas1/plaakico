import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { APIConstant } from "../constant/api.constant";
import { Observable } from "rxjs/Observable";
import { Utility } from "../utility";


@Injectable()
export class TransactionService {

    constructor(private baseService: BaseService) {

    }

    getRecentTransactions(): Observable<any> {
        return this.baseService.get(`${APIConstant.recentTransaction}`);
    }

    getTransactions(params: any): Observable<any> {
        let stringParams = Utility.convertObjectToParams(params);
        return this.baseService.getWithResponse(`${APIConstant.transactions}?${stringParams}`);
    }

    buyUSD(amount: number, token: string, referralCode: string): Observable<any> {
        let url = `${APIConstant.buyUSD}/${amount}/${token}`;
        if (!Utility.isEmpty(referralCode)) {
            url = `${url}?ref=${referralCode}`;
        }
        return this.baseService.get(url);
    }

    buyCrypto(padiAmount: number, buyerCurrency: string, referralCode: string): Observable<any> {
        let url = `${APIConstant.buyCrypto}/${padiAmount}/${buyerCurrency}`;
        if (!Utility.isEmpty(referralCode)) {
            url = `${url}?ref=${referralCode}`;
        }
        return this.baseService.get(url);
    }

}