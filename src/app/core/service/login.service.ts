import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { APIConstant } from "../constant/api.constant";
import { Observable } from "rxjs/Observable";
import { Utility } from "../utility";


@Injectable()
export class LoginService {

    constructor(private baseService: BaseService) {

    }

    login(data: any): Observable<any> {
        return this.baseService.post(`${APIConstant.login}`, data);
    }
    restPassword(data: any): Observable<any> {
        return this.baseService.post(`${APIConstant.restPassword}`, data);
    }
    restPasswordFinal(data: any): Observable<any> {
        return this.baseService.post(`${APIConstant.restPasswordFinal}`, data);
    }

    register(user: any, referralCode: string): Observable<any> {
        let url = `${APIConstant.register}`;
        if (!Utility.isEmpty(referralCode)) {
            url = `${url}/${referralCode}`;
        }
        return this.baseService.post(url, user);
    }

    activate(key: string): Observable<any> {
        return this.baseService.get(`${APIConstant.activateEmail}?key=${key}`);
    }
}