import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/filter'
import 'rxjs/add/operator/map'
import { NotificationsService } from 'angular2-notifications';

@Injectable()
export class MessageService {

    defaultOptions: any = {
        timeOut: 5000,
        showProgressBar: true,
        pauseOnHover: false,
        maxLength: 10,
        clickToClose: true,
        position: ['top', 'right']
    };

    constructor(private _notificationsService: NotificationsService) {

    }

    private create(title: string, message: string, type: string, options?: any) {
        let option = Object.assign({}, this.defaultOptions);
        if (options) {
            option = Object.assign({}, option, options);
        }
        this._notificationsService.create(title, message, type, { ...option });
    }

    success(title: string, message: string, options?: any) {
        this.create(title, message, 'success', options);
    }

    error(title: string, message: string, options?: any) {
        this.create(title, message, 'error', options);
    }

    warn(title: string, message: string, options?: any) {
        this.create(title, message, 'warn', options);
    }

    alert(title: string, message: string, options?: any) {
        this.create(title, message, 'alert', options);
    }

    info(title: string, message: string, options?: any) {
        this.create(title, message, 'info', options);
    }

}