import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { APIConstant } from "../constant/api.constant";
import { Observable } from "rxjs/Observable";


@Injectable()
export class AccountTypeService {

    constructor(private baseService: BaseService) {

    }

    getAccountTypes(): Observable<any> {
        return this.baseService.get(`${APIConstant.accountTypes}`);
    }

}