import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { APIConstant } from "../constant/api.constant";
import { Observable } from "rxjs/Observable";


@Injectable()
export class UserService {

    constructor(private baseService: BaseService) {

    }

    getUser(): Observable<any> {
        return this.baseService.get(`${APIConstant.user}`);
    }

    saveUser(user: any): Observable<any> {
        return this.baseService.post(`${APIConstant.user}`, user);
    }

    changePassword(password: string): Observable<any> {
        return this.baseService.post(`${APIConstant.changePassword}`, password);
    }

    getUserAccounts(): Observable<any> {
        return this.baseService.get(`${APIConstant.userAccounts}`);
    }

    createUserAccounts(user: any): Observable<any> {
        return this.baseService.post(`${APIConstant.userAccounts}`, user);
    }

    updateUserAccounts(user: any): Observable<any> {
        return this.baseService.put(`${APIConstant.userAccounts}`, user);
    }

}