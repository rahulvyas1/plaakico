import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { APIConstant } from "../constant/api.constant";
import { Observable } from "rxjs/Observable";


@Injectable()
export class SupportService
{
    constructor(private baseService: BaseService) {
    }

    createSupportTicket(data: any): Observable<any> {
        return this.baseService.post(`${APIConstant.createSupportTicket}`, data);
    }
}