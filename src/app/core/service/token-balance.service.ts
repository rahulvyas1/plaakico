import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { APIConstant } from "../constant/api.constant";
import { Observable } from "rxjs/Observable";


@Injectable()
export class TokenBalanceService {

    constructor(private baseService: BaseService) {
    }

    getUserTokenBalance(address: string): Observable<any> {
        return this.baseService.get(`${APIConstant.tokenBalance}/?addr=${address}`);
    }

    getTokenRate(): Observable<any> {
        return this.baseService.get(`${APIConstant.tokenRate}`);
    }

}