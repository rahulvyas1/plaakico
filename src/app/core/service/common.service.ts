import { Injectable } from "@angular/core";
import { CommonConstant } from "../constant/common.constant";
import { Utility } from "../utility";

@Injectable()
export class CommonService {

    constructor() {
    }

    //Referral Code

    saveReferralCode(cookie: string) {
        window.localStorage.setItem(CommonConstant.referralCode, cookie);
    }

    getReferralCode(): string {
        return window.localStorage.getItem(CommonConstant.referralCode);
    }
}