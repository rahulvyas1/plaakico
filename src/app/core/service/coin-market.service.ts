import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { APIConstant } from "../constant/api.constant";
import { Observable } from "rxjs/Observable";


@Injectable()
export class CoinMarketService {

    constructor(private baseService: BaseService) {

    }

    getCoinMarketValue(coin: string): Observable<any> {
        return this.baseService.post(`${APIConstant.coinMarketValue}?name=${coin}`, null);
    }

}