import { Injectable } from "@angular/core";
import { CommonConstant } from "../constant/common.constant";
import { Utility } from "../utility";
import { User } from "../../model/user";

@Injectable()
export class AuthService {

    constructor() {
    }

    saveToken(token: string) {
        window.localStorage.setItem(CommonConstant.token, token);
    }

    saveUser(user: User) {
        window.localStorage.setItem(CommonConstant.userInfo, JSON.stringify(user));
    }

    getToken(): string {
        return window.localStorage.getItem(CommonConstant.token);
    }

    getUser(): User {
        let user = window.localStorage.getItem(CommonConstant.userInfo);
        if (!Utility.isEmpty(user)) {
            return JSON.parse(user);
        }
        return null;
    }

    isLoggedIn(): boolean {
        return !Utility.isEmpty(this.getToken());
    }

    loggedOut(): void {
        window.localStorage.clear();
    }

}