import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { APIConstant } from "../constant/api.constant";
import { Observable } from "rxjs/Observable";


@Injectable()
export class PlaakBalanceService {

    constructor(private baseService: BaseService) {

    }

    getPLAAKBalance(contractaddress:string,address: string): Observable<any> {
        return this.baseService.get(`${APIConstant.plaakBalance}contractaddress=${contractaddress}&address=${address}&tag=latest&apikey=${APIConstant.EthplorerApiKey}`);
    }



}