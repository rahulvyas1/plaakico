import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { APIConstant } from "../constant/api.constant";
import { Observable } from "rxjs/Observable";


@Injectable()
export class CoinPaymentService {

    constructor(private baseService: BaseService) {

    }

    getCoinPaymentRates(): Observable<any> {
        return this.baseService.get(`${APIConstant.coinPaymentRates}`);
    }

}