

import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './service/auth-guard.service';
import { LoginService } from './service/login.service';
import { BaseService } from './service/base.service';
import { AuthService } from './service/auth.service';
import { HttpModule } from '@angular/http';
import { EventService } from './event/event.service';
import { MessageService } from './service/message.service';
import { UserService } from './service/user.service';
import { SharedModule } from '../shared/shared.module';
import { CoinPaymentService } from './service/coin-payment.service';
import { TransactionService } from './service/transaction.service';
import { TokenBalanceService } from './service/token-balance.service';
import { AccountTypeService } from './service/account-type.service';
import { ReferralService } from './service/referral.service';
import { PlaakBalanceService } from './service/plaak-balance.service';
import { SupportService } from './service/support.service';
import { CoinMarketService } from './service/coin-market.service';
import { CommonService } from './service/common.service';

@NgModule({
    imports: [
        HttpModule,
        HttpClientModule
    ],
    providers: [
        EventService,
        AuthGuard,
        CommonService,
        LoginService,
        BaseService,
        AuthService,
        MessageService,
        UserService,
        CoinPaymentService,
        TransactionService,
        TokenBalanceService,
        AccountTypeService,
        ReferralService,
        PlaakBalanceService,
        SupportService,
        CoinMarketService
    ]
})
export class CoreModule { }
