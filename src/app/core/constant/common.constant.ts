let PROBLEM_BASE_URL = 'http://www.jhipster.tech/problem';
export const CommonConstant = {
    languageKey: 'en',
    token: 'token',
    userInfo: 'userInfo',
    referralCode: 'ref',
    jwt: 'jwt',
    EMAIL_ALREADY_USED_TYPE: `${PROBLEM_BASE_URL}/email-already-used`,
    LOGIN_ALREADY_USED_TYPE: `${PROBLEM_BASE_URL}/login-already-used`,
    EMAIL_NOT_FOUND_TYPE: `${PROBLEM_BASE_URL}/email-not-found`,
    USD: 'USD',
    ETH: 'ETH',
    pageSize: 10,
    MONTHS: {
        0: "Jan",
        1: "Feb",
        2: "Mar",
        3: "Apr",
        4: "May",
        5: "June",
        6: "July",
        7: "Aug",
        8: "Sep",
        9: "Oct",
        10: "Nov",
        11: "Dec"
    }
};
