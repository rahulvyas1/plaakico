import { environment } from "../../../environments/environment";

let basePath = environment.APIPath;
let nodeAPIPath = environment.NodeAPIPath;
let PLAAKAPIPath = environment.PLAAKAPIPath;
let EthplorerPath = environment.EthplorerPath;
let EthplorerApiKey='BDJJPKQMANARU6TII6WE8XE5GQCAPQ8EG1';
export const APIConstant = {
    basePath: basePath,
    EthplorerApiKey:EthplorerApiKey,
    login: `${basePath}authenticate`,
    restPassword: `${basePath}account/reset-password/init`,
    restPasswordFinal: `${basePath}account/reset-password/finish`,
    register: `${basePath}register`,
    activateEmail: `${basePath}activate`,
    accountTypes: `${basePath}account-types`,
    user: `${basePath}account`,
    changePassword: `${basePath}/account/change-password`,
    userAccounts: `${basePath}user-accounts`,
    coinPaymentRates: `${basePath}coin-payments-rates`,
    recentTransaction: `${basePath}transactions?page=0&size=10&sort=id,asc`,
    transactions: `${basePath}transactions`,
    buyUSD: `${basePath}tokens/buy/usd`,
    buyCrypto: `${basePath}tokens/buy/crypto`,
    tokenBalance: `${nodeAPIPath}`,
    referral: `${basePath}account/referral`,
    //plaakBalance: `${PLAAKAPIPath}plk/balance`,
    plaakBalance: `${EthplorerPath}module=account&action=tokenbalance&`,
    createSupportTicket: `${basePath}public/support-tickets`,
    coinMarketValue: `${PLAAKAPIPath}util/market`,
    tokenRate: `${basePath}public/get-token-rate`,
    tokenTransactionsboughtUserReferral: `${basePath}token-transactions/total-token-bought-by-logged-in-user`,
    tokenTransactionsUserReferral:`${basePath}token-transactions-User-referral`

}