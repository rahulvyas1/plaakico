import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../core/service/auth.service';
import { User } from '../../model/user';
import { EventService } from '../../core/event/event.service';
import { EventConstant } from '../../core/event/event-constant';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
	selector: 'header-bar',
	templateUrl: './header-bar.component.html',
	styleUrls: ['./header-bar.component.css']
})
export class HeaderBarComponent implements OnInit, OnDestroy {


	dropdown = "none";
	user: User;
	sub: any;
	constructor(private router: Router, private authService: AuthService,
		private eventService: EventService) {
		this.subscribe();
	}

	ngOnInit() {
		this.getUser();
	}

	subscribe() {
		this.sub = this.eventService.on(EventConstant.usersaved)
			.subscribe(data => {
				this.getUser();
			});
	};

	getUser() {
		this.user = this.authService.getUser();
	}

	openUserSettings() {
		this.dropdown = this.dropdown === 'none' ? 'block' : 'none';
	}

	outsideClick() {
		if (this.dropdown === 'block') {
			this.dropdown = 'none';
		}
	}

	logout() {
		this.authService.loggedOut();
		this.router.navigate(['login']);
	}

	ngOnDestroy(): void {
		this.sub.unsubscribe();
	}
}