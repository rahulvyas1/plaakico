import { Component, OnInit } from '@angular/core';
import { CommonConstant } from '../../core/constant/common.constant';
import { TransactionService } from '../../core/service/transaction.service';
import { Transaction } from '../../model/transaction';

@Component({
    selector: 'app-transactions',
    templateUrl: './transactions.component.html',
    styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
    loading=false;
    transactions: Transaction[] = [];
    totalItems: number = 0;
    search: any = {
        page: 0,
        size: CommonConstant.pageSize,
        sort: 'transactionDate',
        order: 'asc'
    };

    columns: Array<any> = [
        { name: 'Date', prop: 'transactionDate' },
        { name: 'Currency', prop: 'buyerCurrency' },
        { name: 'From Address', prop: 'fromAddress' },
        { name: 'To Address', prop: 'toAddress' },
        { name: 'Amount', prop: 'amount' },
        { name: 'Status', prop: 'status' }
    ];

    constructor(private transactionService: TransactionService) { }

    ngOnInit() {
        this.getTransactions();
    }

    getTransactions() {
        // this.loading=true;
        let search = this.search;
        search.sort = `${search.sort},${search.order}`;
        delete search.order;
        this.transactionService.getTransactions(search)
            .subscribe(result => {
                // this.loading=false;
                this.transactions = result.body;
                this.totalItems = result.headers.get('X-Total-Count');
            })
    }

    setPage(pageInfo) {
        this.search.page = pageInfo.offset;
        this.getTransactions();
    }

    onSort(event) {
        this.search.page = 0;
        const sort = event.sorts[0];
        this.search.sort = sort.prop;
        this.search.order = sort.dir;
        this.getTransactions();
    }

}
