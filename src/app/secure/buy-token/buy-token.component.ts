import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { StripeCardComponent, ElementsOptions, StripeService, ElementOptions } from 'ngx-stripe';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Utility } from '../../core/utility';
import { CommonConstant } from '../../core/constant/common.constant';
import { TransactionService } from '../../core/service/transaction.service';
import { MessageService } from '../../core/service/message.service';
import { AccountTypeService } from '../../core/service/account-type.service';
import { UserService } from '../../core/service/user.service';
import { AccountType } from '../../model/account-type';
import { UserAccount } from '../../model/user-account';
import { ValidationService } from '../../shared/components/validation-message/validation-service';
import { CommonService } from '../../core/service/common.service';

@Component({
    selector: 'app-buy-token',
    templateUrl: './buy-token.component.html',
    styleUrls: ['./buy-token.component.css']
})
export class BuyTokenComponent implements OnInit {

    @ViewChild(StripeCardComponent) card: StripeCardComponent;
    coin: string;
    isUSD: boolean;
    isNonUSD: boolean;
    showAddress: boolean;
    loading: boolean;
    tokens: number = 0;
    amountCrypto: number;
    cryptoAddress: any;
    hasDataLoad: boolean;
    cardOptions: ElementOptions = {
        style: {
            base: {
                color: '#32325d',
                lineHeight: '18px',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        },
        hidePostalCode: true
    };
    elementsOptions: ElementsOptions = {
        locale: 'en'
    };
    stripeTest: FormGroup;
    frmBuyCrypto: FormGroup;
    frmAddress: FormGroup;
    accountTypes: AccountType[];
    hasUserAccount: boolean;
    amount: number;
    referralCode: string;
    confirmAns: string;
    errorMessage: string;

    constructor(private route: ActivatedRoute, private router: Router,
        private fb: FormBuilder, private stripeService: StripeService,
        private userSerive: UserService, private accountTypeService: AccountTypeService,
        private transactionService: TransactionService, private messageService: MessageService,
        private commonService: CommonService) { }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.coin = params['coin'];

            if (!Utility.isEmpty(this.coin)) {
                this.isUSD = this.coin.toLowerCase() === CommonConstant.USD.toLowerCase();
                this.isNonUSD = !this.isUSD;
            }
        });
        this.createStripeForm();
        this.createBuyCryptoForm();
        this.createAddressForm();
        this.getAccountTypes();
        this.getReferralCode();
    }

    getAccountTypes() {
        this.accountTypeService.getAccountTypes()
            .subscribe(result => {
                this.accountTypes = result;
                this.getUserAccount();
            })
    }

    getUserAccount() {
        this.userSerive.getUserAccounts()
            .subscribe((result: UserAccount[]) => {
                this.hasDataLoad = true;
                this.hasUserAccount = !Utility.isEmpty(result) && !Utility.isEmpty(result.find(x => x.accountType.name === CommonConstant.ETH));
            }, error => {
                this.messageService.error('User Account', 'Error while fetching data');
            })
    }

    getReferralCode() {
        this.referralCode = this.commonService.getReferralCode();
    }

    createAddressForm() {
        this.frmAddress = this.fb.group({
            address: ['', [Validators.required, ValidationService.validAddress]],
            confirmAns: ['']
        });
    }

    createStripeForm() {
        this.stripeTest = this.fb.group({
            name: ['', [Validators.required]],
            amount: ['', [Validators.min(30), Validators.max(300000)]]
        });
    }

    createBuyCryptoForm() {
        this.frmBuyCrypto = this.fb.group({
            amount: ['', [Validators.required, Validators.min(30), Validators.max(300000)]]
        });
    }

    buyUSD() {
        this.loading = true

        const name = this.stripeTest.get('name').value;
        const amount = this.stripeTest.get('amount').value;

        this.stripeService
            .createToken(this.card.getCard(), { name })
            .subscribe((result) => {
                if (result.token) {
                    this.transactionService.buyUSD(amount, result.token.id, this.referralCode).subscribe((success) => {
                        this.loading = false;
                        this.tokens += success.tokens;
                        this.amount = success.amount;
                        this.messageService.success('Token', `Successfully bought ${success.tokens} Tokens`);
                    }, (error) => {
                        this.loading = false;
                        this.errorMessage = result.error.message;
                        //this.messageService.error('Token', error.error.title);
                    });
                } else if (result.error) {
                    // Error creating the token
                    this.loading = false;
                    this.errorMessage = result.error.message;

                    //this.messageService.error('Token', result.error.message);
                }
            }, (error) => {
                this.loading = false
            });
    }

    buyCrypto() {
        if (!this.frmBuyCrypto.valid) {
            return;
        }
        this.loading = true;
        let amount = this.frmBuyCrypto.value.amount;
        this.transactionService.buyCrypto(amount, this.coin, this.referralCode).subscribe((address) => {
            this.loading = false;
            this.isNonUSD = false;
            this.showAddress = true;
            this.cryptoAddress = address;
            this.amount = address.amount;
        }, (error) => {
            this.loading = false;
            this.errorMessage = error.error.title;
            //this.messageService.error('Token', error.error.title);
        });
    }

    getAddress() {
        let address = this.frmAddress.value.address;
        if (!address.startsWith('0x')) {
            address = `0x${address}`;
        }
        return address;
    }

    sendCoin() {
        this.router.navigate(['dashboard']);
    }

    saveUserAccount() {
        if (this.frmAddress.invalid) {
            return;
        }

        let address = this.frmAddress.value.address;
        if (!address.startsWith('0x')) {
            address = `0x${address}`;
        }
        let user: UserAccount = {
            accountType: this.accountTypes.find(x => x.name.toLowerCase() === CommonConstant.ETH.toLowerCase()),
            balance: 0,
            external: true,
            openDate: new Date(),
            address: address
        }
        this.userSerive.createUserAccounts(user)
            .subscribe(result => {
                this.hasUserAccount = true;
            }, error => {
                this.messageService.error('User Accounts', 'Error while saving user.')
            })

    }

}
