import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

@Component({
    selector: 'secure',
    templateUrl: './secure.component.html',
    styleUrls: ['./secure.component.css']
})

export class SecureComponent implements OnDestroy {

    subRoute: any;
    currentPage: string;
    isReferal: boolean;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private titleService: Title) {
        this.subRoute = this.router.events
            .filter((event) => event instanceof NavigationEnd)
            .map(() => this.activatedRoute)
            .map((route) => {
                while (route.firstChild) route = route.firstChild;
                return route;
            })
            .filter((route) => route.outlet === 'primary')
            .mergeMap((route) => route.data)
            .subscribe((event) => {
                this.currentPage = event.title;
                this.titleService.setTitle(this.currentPage);
            });

    };

    ngOnDestroy(): void {
        this.subRoute.unsubscribe();
    }
}