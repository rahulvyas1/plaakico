import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SecureComponent } from './secure.component';
import { AuthGuard } from '../core/service/auth-guard.service';
import { BuyTokenComponent } from './buy-token/buy-token.component';
import { ReferralComponent } from './referral/referral.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { SupportComponent } from './support/support.component';
import { TandcComponent } from './terms/terms.component';
//routes
const routes: Routes = [
    {
        path: '',
        component: SecureComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'dashboard', component: DashboardComponent, data: { title: 'Dashboard' } },
            { path: 'buy-token', component: BuyTokenComponent, data: { title: 'Buy Token' } },
            { path: 'buy-token/:coin', component: BuyTokenComponent, data: { title: 'Buy Token' } },
            { path: 'referral', component: ReferralComponent, data: { title: 'Referral' } },
            { path: 'transactions', component: TransactionsComponent, data: { title: 'Transactions' } },
            { path: 'support', component: SupportComponent, data: { title: 'Support' } },
            { path: 'terms', component: TandcComponent, data: { title: 'Terms and Conditions' } },
            { path: 'settings', loadChildren: 'app/secure/settings/settings.module#SettingsModule', data: { title: 'Settings' } },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SecureRoutingModule { }

export const SecureComponents = [DashboardComponent, SecureComponent, BuyTokenComponent, SupportComponent, TransactionsComponent, TandcComponent];
