import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from '../../core/service/auth.service';
import { User } from '../../model/user';
import { SupportService } from '../../core/service/support.service';

@Component({
    selector: 'app-supports',
    templateUrl: './support.component.html',
    styleUrls: ['./support.component.css']
})
export class SupportComponent implements OnInit 
{
	frmSupport: FormGroup;
	user: User;
	ticketSuccess : boolean = false;
	constructor(private formBuilder: FormBuilder,private authService: AuthService,private supportService:SupportService)
    {
    	this.createSupportForm();
    }

    ngOnInit() {
    }

    createSupportForm() {
		this.frmSupport = this.formBuilder.group({
			topic: ['', [Validators.required]],
			message: ['', [Validators.required]]
		});
	}

	sendSupport()
	{
		if (this.frmSupport.invalid) 
		{
			return;
		}
		let user = this.frmSupport.value;
		
		this.user = this.authService.getUser();

		var obj = {
			"email": this.user.email,
			"message": user.message,
			"name": this.user.firstName+" "+this.user.lastName,
			"subject": user.topic
		};

		this.supportService.createSupportTicket(obj)
		.subscribe(ticket_result => 
		{
			console.log(ticket_result);
			
			this.ticketSuccess = true;

			setTimeout(()=>
			{
				this.ticketSuccess = false;
				this.createSupportForm();
		 	},5000);
		},
		error => {

			
		});

	}
}
