import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/service/auth.service';
import { User } from '../../model/user';
import { CoinPaymentService } from '../../core/service/coin-payment.service';
import { CoinPaymentRates, CoinPaymentRatesObject } from '../../model/coin-payment-rates';
import { Transaction } from '../../model/transaction';
import { TransactionService } from '../../core/service/transaction.service';
import { UserService } from '../../core/service/user.service';
import { UserAccount } from '../../model/user-account';
import { TokenBalanceService } from '../../core/service/token-balance.service';
import { Router } from '@angular/router';
import { Utility } from '../../core/utility';
import { CommonConstant } from '../../core/constant/common.constant';
import { PlaakBalanceService } from '../../core/service/plaak-balance.service';
import { CoinMarketService } from '../../core/service/coin-market.service';
import { BaseService } from '../../core/service/base.service';
import { APIConstant } from '../../core/constant/api.constant';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  user: User;
  coinPaymentRates: CoinPaymentRatesObject;
  tempCoinPaymentRates: CoinPaymentRatesObject;
  transactions: Transaction[];
  hasAccountConfigured: boolean;
  usd: string = CommonConstant.USD.toLowerCase();
  plaakBalance: string;
  month: number;
  year: number;
  date: number;
  message: string = "";
  cardCurrecies: { [key: string]: any } = {};
  tokenRate: number = 1;

  constructor(
    private authService: AuthService, private coinPaymentService: CoinPaymentService,
    private transactionService: TransactionService, private userService: UserService,
    private tokenBalanceService: TokenBalanceService, private router: Router,
    private plaakBalanceService: PlaakBalanceService, private coinMarketService: CoinMarketService) {

  }

  ngOnInit() {
    this.setMessage();
    this.getUser();
    this.getUserAccounts();
    this.getCoinPaymentRates();
    this.getTransactions();
    this.getTokenRate();
  }

  setMessage() {
    var d = new Date();
    this.month = CommonConstant.MONTHS[d.getMonth()];
    this.year = d.getFullYear();
    this.date = d.getDate();
    var curHr = d.getHours()
    if (curHr < 12) {
      this.message = "Good Morning";
    } else if (curHr < 18) {
      this.message = "Good Afternoon";
    }
    else {
      this.message = "Good Evening";
    }
  }

  getTokenRate() {
    this.tokenBalanceService.getTokenRate()
      .subscribe(result => {
        this.tokenRate = result.tokenRate;

        this.getCardCurrenciesData();
      })
  }

  getCardCurrenciesData() {
    this.cardCurrecies = {
      'Bitcoin': {
      }, 'Ethereum': {
      }
    };

    for (let key in this.cardCurrecies) {
      this.coinMarketService.getCoinMarketValue(key)
        .subscribe(result => {
          if (!Utility.isEmpty(result)) {
            this.cardCurrecies[key].symbol = result[0].symbol;
            this.cardCurrecies[key].USDAmount = result[0].price_usd;
            this.cardCurrecies[key].tokenAmount = (result[0].price_usd / this.tokenRate).toFixed(2);
            this.cardCurrecies[key].dailyAverage = +result[0].percent_change_24h;
          }
        })
    }
  }

  getUser() {
    this.user = this.authService.getUser();
  }

  getUserAccounts() {
    this.userService.getUserAccounts()
      .subscribe((result: UserAccount[]) => {

        if (Utility.isEmpty(result)) {
          this.hasAccountConfigured = false;
          return;
        }
        let temp = result.find(x => x.accountType.name === CommonConstant.ETH);
        if (Utility.isEmpty(temp)) {
          this.hasAccountConfigured = false;
          return;
        }
        this.hasAccountConfigured = true;
        let account: UserAccount = temp;
        console.log(account.address);
        this.getPLAAKBalance('0x96fc0229b1aab013000909143f079ab096d55317',account.address);
      }, error => {
      });
  }

  // getPLAAKBalance(address: string) {
  //   this.baseService.get(`${APIConstant.plaakBalance}?addr=${address}`)
  //     .subscribe((result:any) => {
  //       console.log(result);

  //       this.plaakBalance = result.balance;
  //     }, error => {
  //       console.log(error);
  //     })
  // }
  getPLAAKBalance(contract :string,address: string) {
    this.plaakBalanceService.getPLAAKBalance(contract, address)
      .subscribe((result: any) => {
        let res = +result.result;
        
          res/=1000000000000000000;
      
        this.plaakBalance = res.toFixed(2).toString();
      }, error => {
        console.log(error);
      })
  }
  getCoinPaymentRates() {
    this.coinPaymentService.getCoinPaymentRates()
      .subscribe((results: CoinPaymentRatesObject) => {
        this.coinPaymentRates = results;
        this.tempCoinPaymentRates = Object.assign({}, results);
      }, error => {

      })
  }

  getTransactions() {
    this.transactionService.getRecentTransactions()
      .subscribe((results: Transaction[]) => {
        this.transactions = results;
      }, error => {

      })
  }

  filterBuy(search: string) {
    if (!search) {
      this.coinPaymentRates = Object.assign({}, this.tempCoinPaymentRates);
    }
    this.coinPaymentRates = {};
    for (var key in this.tempCoinPaymentRates) {
      if (this.tempCoinPaymentRates.hasOwnProperty(key)) {
        if (key.toLowerCase().indexOf(search.toLowerCase()) > -1)
          this.coinPaymentRates[key] = this.tempCoinPaymentRates[key];
      }
    }
  }

  buyToken(coin: string) {
    this.router.navigate(['buy-token', coin]);

  }
}
