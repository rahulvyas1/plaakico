import { NgModule } from '@angular/core';
import { SecureRoutingModule, SecureComponents } from './secure-routing.module';
import { HeaderBarComponent } from './header-bar/header-bar.component';
import { SharedModule } from '../shared/shared.module';
import { NgxStripeModule } from 'ngx-stripe';
import { environment } from '../../environments/environment';
import { LoadingModule } from 'ngx-loading';
import { ReferralComponent } from './referral/referral.component';
import { ClipboardModule } from 'ngx-clipboard';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { QRCodeModule } from 'angular2-qrcode';

@NgModule({
    declarations: [
        SecureComponents,
        ReferralComponent
    ],
    imports: [
        SecureRoutingModule,
        SharedModule,
        NgxStripeModule.forRoot(environment.stripeKey),
        LoadingModule,
        ClipboardModule,
        NgxDatatableModule,
        QRCodeModule
    ],
    providers: [
    ]
})
export class SecureModule { }
