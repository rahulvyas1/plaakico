import { Component, OnInit } from '@angular/core';
import { ReferralService } from '../../core/service/referral.service';
import { CommonConstant } from '../../core/constant/common.constant';
import { BaseService } from '../../core/service/base.service';
import { APIConstant } from '../../core/constant/api.constant';

@Component({
  selector: 'app-referral',
  templateUrl: './referral.component.html',
  styleUrls: ['./referral.component.css']
})
export class ReferralComponent implements OnInit {

  referralCode: string;
  referralURL: string;
  isCopied: boolean;
  referredUsers;
  totalReferrals: number = 0;
  constructor(private baseService: BaseService, private referralService: ReferralService) { }

  ngOnInit() {
    this.getAllReferralTokenTransactionForLoggedInUser();
    this.getReferralData();
  }

  getTotalPEKEaned(): any {
    //return this.baseService.get(`${APIConstant.accountTypes}`);
    this.baseService.get(`${APIConstant.tokenTransactionsboughtUserReferral}`).subscribe(data => {
      console.log(data);

    });
  }
  getAllReferralTokenTransactionForLoggedInUser() {
  //   const data = [  
  //     {  
  //        "id":18,
  //        "status":"pending",
  //        "type":"REFERRAL",
  //        "transactionDate":"2018-01-24T09:08:13Z",
  //        "userLogin":"rahul.class@gmail.com",
  //        "msg":null,
  //        "tokens":100.0,
  //        transaction:{  
  //           "id":10,
  //           "transactionDate":"2018-01-24T08:42:05Z",
  //           "amount":0.010066416573600473,
  //           "fromAddress":"User Account",
  //           "toAddress":"0xd8dfd39f05d0b7748caa65d012a8cd7e98bb5588",
  //           "tokens":100.0,
  //           "status":"Complete",
  //           "buyerCurrency":"ETH",
  //           "tokenUsdRate":0.10000000149011612,
  //           "userLogin":"charissa_22@hotmail.com",
  //           "notes":null,
  //           "ethAddress":"",
  //           "firstName":null,
  //           "lastName":null
  //        },
  //        "txid":null
  //     },
  //     {  
  //        "id":20,
  //        "status":"pending",
  //        "type":"REFERRAL",
  //        "transactionDate":"2018-01-24T09:19:13Z",
  //        "userLogin":"rahul.class@gmail.com",
  //        "msg":null,
  //        "tokens":100.0,
  //        "transaction":{  
  //           "id":10,
  //           "transactionDate":"2018-01-24T08:42:05Z",
  //           "amount":0.010066416573600473,
  //           "fromAddress":"User Account",
  //           "toAddress":"0xd8dfd39f05d0b7748caa65d012a8cd7e98bb5588",
  //           "tokens":100.0,
  //           "status":"Complete",
  //           "buyerCurrency":"ETH",
  //           "tokenUsdRate":0.10000000149011612,
  //           "userLogin":"charissa_22@hotmail.com",
  //           "notes":null,
  //           "ethAddress":"",
  //           "firstName":null,
  //           "lastName":null
  //        },
  //        "txid":null
  //     },
  //     {  
  //        "id":24,
  //        "status":"pending",
  //        "type":"REFERRAL",
  //        "transactionDate":"2018-01-28T08:32:10Z",
  //        "userLogin":"rahul.class@gmail.com",
  //        "msg":null,
  //        "tokens":3.0,
  //        "transaction":{  
  //           "id":21,
  //           "transactionDate":"2018-01-28T07:57:32Z",
  //           "amount":0.002589966645158099,
  //           "fromAddress":"User Account",
  //           "toAddress":"0xa299828ac3fab20d6ab8e4602c35e12aa78bbe1a",
  //           "tokens":30.0,
  //           "status":"Complete",
  //           "buyerCurrency":"ETH",
  //           "tokenUsdRate":0.10000000149011612,
  //           "userLogin":"dbrobson79@gmail.com",
  //           "notes":null,
  //           "ethAddress":"0xa4B1FE7dd77248FACBCC93EcD7760BD8DD8e7eFd",
  //           "firstName":null,
  //           "lastName":null
  //        },
  //        "txid":null
  //     },
  //     {  
  //        "id":27,
  //        "status":"pending",
  //        "type":"REFERRAL",
  //        "transactionDate":"2018-01-29T03:41:14Z",
  //        "userLogin":"rahul.class@gmail.com",
  //        "msg":null,
  //        "tokens":3.0,
  //        "transaction":{  
  //           "id":23,
  //           "transactionDate":"2018-01-29T03:18:42Z",
  //           "amount":0.2299455955782522,
  //           "fromAddress":"User Account",
  //           "toAddress":"LMAheA495g7Z9gbagnhBwCSJH9Cb3DLsxL",
  //           "tokens":30.0,
  //           "status":"Complete",
  //           "buyerCurrency":"LTC",
  //           "tokenUsdRate":1.399999976158142,
  //           "userLogin":"dbrobson79@gmail.com",
  //           "notes":null,
  //           "ethAddress":"0xa4B1FE7dd77248FACBCC93EcD7760BD8DD8e7eFd",
  //           "firstName":"Damian ",
  //           "lastName":"Robson"
  //        },
  //        "txid":null
  //     },
  //     {  
  //        "id":53,
  //        "status":"initiated",
  //        "type":"REFERRAL",
  //        "transactionDate":"2018-01-30T13:54:17Z",
  //        "userLogin":"rahul.class@gmail.com",
  //        "msg":"",
  //        "tokens":3.0,
  //        "transaction":{  
  //           "id":48,
  //           "transactionDate":"2018-01-30T13:37:48Z",
  //           "amount":0.2425797461589057,
  //           "fromAddress":"User Account",
  //           "toAddress":"LPhfRkRg56aBnwGzWDNUPwdmS8xVYDR7H9",
  //           "tokens":30.0,
  //           "status":"Complete",
  //           "buyerCurrency":"LTC",
  //           "tokenUsdRate":1.399999976158142,
  //           "userLogin":"dbrobson79@gmail.com",
  //           "notes":null,
  //           "ethAddress":"0xa4B1FE7dd77248FACBCC93EcD7760BD8DD8e7eFd",
  //           "firstName":"Damian ",
  //           "lastName":"Robson"
  //        },
  //        "txid":"0x1ccc1affc84c540d2d1b7f3b65f4482c790db57b12d76d053b1b2a26e1c41ef6"
  //     }
  //  ];
     this.baseService.get(`${APIConstant.tokenTransactionsUserReferral}`).subscribe((data:any)=>{
    console.log(data);
    if (data && data.length > 0) {
      this.referredUsers = data.filter(e=>e.status==='initiated' && e.transaction.status==='Complete')
      .map(e=>Object.assign({},e,{
DisplayEmail:e.transaction.userLogin.substring(0,5)+'*******'
      }));
      let total = 0;
      this.referredUsers.forEach(element => {        
            total +=   element.tokens;        
      });
      this.totalReferrals = total;
    }

     });
  }

  getReferralData() {
    this.referralService.getAccountReferral()
      .subscribe(result => {
        this.referralCode = result.refferal;
        this.referralURL = `${window.location.origin}/ico/?${CommonConstant.referralCode}=${this.referralCode}`
      })
  }

}
