import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../core/service/user.service';
import { MessageService } from '../../../core/service/message.service';
import { User } from '../../../model/user';
import { AuthService } from '../../../core/service/auth.service';
import { FormGroup } from '@angular/forms/src/model';
import { FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from '../../../shared/components/validation-message/validation-service';

@Component({
    selector: 'user-settings',
    templateUrl: './user-settings.component.html',
    styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent implements OnInit {

    user: User;
    frmUser: FormGroup;
    isFormSubmitted: boolean;

    constructor(private userSerive: UserService, private authService: AuthService,
        private messageService: MessageService, private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.createForm();
        this.getUser();
    }

    createForm() {
        this.frmUser = this.formBuilder.group({
            phoneNumber: ['', [Validators.required]],
            firstName: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
            lastName: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
            email: ['', [Validators.required, ValidationService.emailValidator]],
        });
    }

    getUser() {
        this.user = this.authService.getUser();
        this.frmUser.setValue({
            firstName: this.user.firstName,
            lastName: this.user.lastName,
            email: this.user.email,
            phoneNumber: this.user.phoneNumber
        });
    }

    save() {
        this.isFormSubmitted = true;
        if (this.frmUser.invalid) {
            return;
        }
        let user: any = {

            id: this.user.id,
            phoneNumber: this.frmUser.value.phoneNumber,
            lastName: this.frmUser.value.lastName,
            firstName: this.frmUser.value.firstName,
            email: this.frmUser.value.email,
            login: this.user.login
        };

        this.userSerive.saveUser(user)
            .subscribe(result => {
                this.user = result;
                this.authService.saveUser(this.user);
                this.messageService.success('User Settings', 'Saved Successfully.')
            }, error => {
                this.messageService.error('User Settings', 'Error while saving user.')
            })
    }

}
