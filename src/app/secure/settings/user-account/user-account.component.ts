import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../core/service/user.service';
import { MessageService } from '../../../core/service/message.service';
import { AuthService } from '../../../core/service/auth.service';
import { FormGroup } from '@angular/forms/src/model';
import { FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from '../../../shared/components/validation-message/validation-service';
import { UserAccount } from '../../../model/user-account';
import { Utility } from '../../../core/utility';
import { AccountType } from '../../../model/account-type';
import { AccountTypeService } from '../../../core/service/account-type.service';
import { CommonConstant } from '../../../core/constant/common.constant';

@Component({
    selector: 'user-account',
    templateUrl: './user-account.component.html',
    styleUrls: ['./user-account.component.css']
})
export class UserAccountComponent implements OnInit {

    userAccount: UserAccount;
    frmUserAccount: FormGroup;
    isFormSubmitted: boolean;
    isEditForm: boolean;
    accountTypes: AccountType[];

    constructor(private userSerive: UserService, private accountTypeService: AccountTypeService,
        private messageService: MessageService, private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.createForm();

        this.getAccountTypes();
    }

    getAccountTypes() {
        this.accountTypeService.getAccountTypes()
            .subscribe(result => {
                this.accountTypes = result;
                this.getUserAccount();
            })
    }

    createForm() {
        this.frmUserAccount = this.formBuilder.group({
            address: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(64), ValidationService.validAddress]],
        });
    }

    setValue() {
        this.frmUserAccount.setValue({
            address: this.userAccount.address,
        });
    }

    getUserAccount() {
        this.userSerive.getUserAccounts()
            .subscribe((result: UserAccount[]) => {
                if (Utility.isEmpty(result) || Utility.isEmpty(result.find(x => x.accountType.name === CommonConstant.ETH))) {
                    this.isEditForm = false;
                } else {
                    let temp = result.find(x => x.accountType.name === CommonConstant.ETH);
                    this.isEditForm = true;
                    this.userAccount = temp;
                    this.setValue();
                }
            }, error => {
                this.messageService.error('User Account', 'Error while fetching data');
            })
    }

    save() {
        this.isFormSubmitted = true;
        if (this.frmUserAccount.invalid) {
            return;
        }

        let address = this.frmUserAccount.value.address;
        if (!address.startsWith('0x')) {
            address = `0x${address}`;
        }
        if (this.isEditForm) {
            this.userAccount.address = address;
            this.userSerive.updateUserAccounts(this.userAccount)
                .subscribe(result => {
                    this.userAccount = result;
                    this.messageService.success('User Settings', 'Saved Successfully.')
                }, error => {
                    this.messageService.error('User Settings', 'Error while saving user.')
                })

        } else {
            let userAccount = {
                accountType: this.accountTypes.find(x => x.name.toLowerCase() === CommonConstant.ETH.toLowerCase()),
                balance: 0,
                external: true,
                openDate: new Date(),
                address: address
            }
            this.userSerive.createUserAccounts(userAccount)
                .subscribe(result => {
                    this.userAccount = result;
                    this.messageService.success('User Settings', 'Saved Successfully.')
                }, error => {
                    this.messageService.error('User Settings', 'Error while saving user.')
                })
        }
    }

}
