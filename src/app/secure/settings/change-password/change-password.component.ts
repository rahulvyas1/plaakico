import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../../core/service/message.service';
import { User } from '../../../model/user';
import { AuthService } from '../../../core/service/auth.service';
import { FormGroup } from '@angular/forms/src/model';
import { FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from '../../../shared/components/validation-message/validation-service';
import { UserService } from '../../../core/service/user.service';

@Component({
    selector: 'change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.css']
})

export class ChangePasswordComponent implements OnInit {

    user: User;
    frmChangePasssword: FormGroup;
    isFormSubmitted: boolean;

    constructor(private userSerive: UserService, private authService: AuthService,
        private messageService: MessageService, private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.createForm();
    }

    createForm() {
        this.frmChangePasssword = this.formBuilder.group({
            password: ['', [Validators.required, Validators.minLength(6)]],
            confirmPassword: ['', [Validators.required, ValidationService.comparePassword]]
        });
    }

    save() {
        this.isFormSubmitted = true;
        if (this.frmChangePasssword.invalid) {
            return;
        }

        this.userSerive.changePassword(this.frmChangePasssword.value.password)
            .subscribe(result => {
                this.messageService.success('Change Password', 'Saved Successfully.')
            }, error => {
                this.messageService.error('Change Password', 'Error while saving change password.')
            })
    }

}
