import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../core/service/auth-guard.service';
import { SettingsComponent } from './settings.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { UserAccountComponent } from './user-account/user-account.component';

//routes
const routes: Routes = [
    {
        path: '',
        component: SettingsComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'user-settings', pathMatch: 'full' },
            { path: 'user-settings', component: UserSettingsComponent, data: { title: 'User Settings' } },
            { path: 'change-password', component: ChangePasswordComponent, data: { title: 'Change Password' } },
            { path: 'user-account', component: UserAccountComponent, data: { title: 'User Account' } },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SettingsRoutingModule { }

export const SettingsComponents = [UserSettingsComponent, SettingsComponent, ChangePasswordComponent,
    UserAccountComponent];
