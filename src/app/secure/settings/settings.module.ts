import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { SettingsComponents, SettingsRoutingModule } from './settings-routing.module';

@NgModule({
    declarations: [
        SettingsComponents
    ],
    imports: [
        SettingsRoutingModule,
        SharedModule,
    ],
    providers: [
    ]
})
export class SettingsModule { }
