import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { EmailActivationComponent } from './email-activation/email-activation.component';
// import { TermsComponent } from './terms-and-conditions/terms.component';


//routes
const routes: Routes = [
  //{ path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'activate', component: EmailActivationComponent },
  // { path: 'terms-conditions', component: TermsComponent },

  { path: '', loadChildren: 'app/secure/secure.module#SecureModule' },
  // { path: 'dashboard', component: DashboardComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
