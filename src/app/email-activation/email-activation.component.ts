import { Component, OnInit } from '@angular/core';
import { LoginService } from '../core/service/login.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Utility } from '../core/utility';

@Component({
    selector: 'email-activation',
    templateUrl: './email-activation.component.html',
    styleUrls: ['./email-activation.component.css']
})
export class EmailActivationComponent implements OnInit {

    errorMessage: string;
    success: boolean;
    key: string;
    constructor(private route: ActivatedRoute, private router: Router, private loginService: LoginService) { }

    ngOnInit() {
        this.route.queryParams.subscribe((params: Params) => {
            this.key = params['key'];

            if (!Utility.isEmpty(this.key)) {
                this.activationKey(this.key);
            } else {
                this.errorMessage = 'Invalid URL';
            }
        });
    }


    activationKey(key: string) {
        this.loginService.activate(key)
            .subscribe(result => {
                this.success = true;
            }, error => {
                this.errorMessage = error.error.title;
            })
    }

    goToLogin() {
        this.router.navigate(['login']);
    }
}