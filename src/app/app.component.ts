import { Component, OnInit } from '@angular/core';
import { CommonConstant } from './core/constant/common.constant';
import { CommonService } from './core/service/common.service';
import { UserService } from './core/service/user.service';
import { AuthService } from './core/service/auth.service';
import { User } from './model/user';
import { Router } from '@angular/router';
import { EventService } from './core/event/event.service';
import { EventConstant } from './core/event/event-constant';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  public options = {
    timeOut: 5000,
    showProgressBar: true,
    pauseOnHover: false,
    maxLength: 10,
    clickToClose: true,
    position: ['top', 'right']
  }

  constructor(private router: Router, private commonService: CommonService, private authService: AuthService,
    private userService: UserService, private eventService: EventService) {
    this.checkJWTToken();
    this.checkReferralCode();
  }

  checkReferralCode() {
    let urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has(CommonConstant.referralCode)) {
      this.commonService.saveReferralCode(urlParams.get(CommonConstant.referralCode));
    }
  }

  checkJWTToken() {
    let urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has(CommonConstant.jwt)) {
      this.authService.saveToken(urlParams.get(CommonConstant.jwt));

      this.userService.getUser()
        .subscribe((user: User) => {
          this.authService.saveUser(user);
          this.eventService.broadcast(EventConstant.usersaved);
          this.router.navigate(['/dashboard']);
        })
    }
  }

}