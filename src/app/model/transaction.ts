import { CryptoTransaction } from "./crypto-transation";

export class Transaction {
    id?: number;
    transactionDate?: any;
    amount?: number;
    fromAddress?: string;
    toAddress?: string;
    tokens?: number;
    userAccount?: any;
    cryptoTransaction: CryptoTransaction;
    status: string;
    stripeTransaction?: any;
}