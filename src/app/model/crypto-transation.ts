export class CryptoTransaction {
    address: string;
    amount: string;
    confirms_needed: string;
    cryptoTransactionInfo: string;
    qrcode_url: string;
    status_url: string;
    timeout: number;
    txn_id: string;
}