import { AccountType } from "./account-type";

export class UserAccount {
    id?: number;
    balance: number;
    address: string;
    openDate: Date;
    external: boolean;
    accountType: AccountType;
    userLoginName?: string;
}