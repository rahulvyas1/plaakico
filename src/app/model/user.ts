export class User {
    activated: boolean;
    authorities: string[];
    createdBy: string;
    createdDate: Date;
    email: string;
    firstName: string;
    id: number;
    imageUrl: string;
    langKey: string;
    lastModifiedBy: string;
    lastModifiedDate: Date;
    lastName: string;
    login: string;
    phoneNumber: string;
    phoneNumberRegion: string;
    referraPercent: number;
    referredBy: string;
}