export class CoinPaymentRates {
    is_fiat: number;
    rate_btc: number;
    last_update: Date;
    tx_fee: number;
    status: string;
    name: string;
    confirms: number;
    can_convert: number;
    capabilities: string[];
}

export class CoinPaymentRatesObject {
    [key: string]: CoinPaymentRates
}